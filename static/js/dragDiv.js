
$(function() {
    $(document).mousemove(function(e) {
        if (!!this.move) {
            let posix = !document.move_target ? {'x': 0, 'y': 0} : document.move_target.posix,
                callback = document.call_down || function() {
                    $(this.move_target).css({
                        'top': e.pageY - posix.y,
                        'left': e.pageX - posix.x
                    });
                };
            callback.call(this, e, posix);
        }
    }).mouseup(function(e) {
        if (!!this.move) {
            let callback = document.call_up || function(){};
            callback.call(this, e);
            $.extend(this, {
                'move': false,
                'move_target': null,
                'call_down': false,
                'call_up': false
            });
        }
    });

    let $box = $('.box').mousedown(function(e) {
        let offset = $(this).offset();

        this.posix = {'x': e.pageX - offset.left, 'y': e.pageY - offset.top};
        $.extend(document, {'move': true, 'move_target': this});
    }).on('mousedown', '.coor', function(e) {
        let fBox = $(this).parent();

        let posix = {
            'w': fBox.width(),
            'h': fBox.height(),
            'x': e.pageX,
            'y': e.pageY
        };

        $.extend(document, {'move': true, 'call_down': function(e) {
                fBox.css({
                    'width': Math.max(30, e.pageX - posix.x + posix.w),
                    'height': Math.max(30, e.pageY - posix.y + posix.h)
                });
            }});
        return false;
    });
});