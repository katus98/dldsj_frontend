#### 项目介绍
 地理所重研-地理大数据时空解析原型系统-前端
 
#### 前端框架

 vue

#### 使用说明

1. clone到本地后需手动安装node_modules，(c)npm install。
2. 可能由于node_modules目录太复杂，打包所需时间太长，建议使用(c)npm run dev运行开发者模式。
3. 缺少依赖项导致的无法启动，可使用(c)npm install xxx --save安装，新增依赖（带版本号）会添加至package.json中。

#### 项目人员

1. 傅智一
2. 楼宇秦
3. 胡林舒
4. 梁钢
3. 李延龙
4. 陈振德
