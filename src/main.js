// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import axios from 'axios'

axios.defaults.withCredentials = true;

import echarts from 'echarts'
import 'echarts/dist/echarts.min.js'
// import jquery from 'jquery'
// import 'jquery/dist/jquery.min.js'
import $ from 'jquery'

import N3Components from 'N3-components'
import 'N3-components/dist/index.min.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Vuelidate from 'vuelidate'
import vueGridLayout from 'vue-grid-layout';

import 'fullpage.js/vendors/scrolloverflow';
import VueFullPage from 'vue-fullpage.js'
import "./style/fullpage.min.css"

import App from './App'
import store from './store'
import util from './util/util'
import resource from './resource'
import {routers} from './router'
import Bus from './util/bus'
import './util/directives'
import URL from './util/url'
import staticData from './util/staticData'
import publicModelOption from './util/publicModelOption'
import {get, patch, post, post1, post2, postAdvanced, put, remove} from './util/axios'
import {delCookie, getCookie, setCookie} from './util/cookie'
import {formatTime, dateFormat} from './util/date'
import {
    toLineChartData,
    toPieChartData,
    toBarChartData,
    toBarbarChartData,
    toGuageChartData
} from './views/layout/changeChart'
import {
    getOptionData,
    getField,
    // getModelData,
    getDataOfOption,
    xmlToJson,
    stringToXml,
    getMyDataField,
    formatFileSize,
    // getTotalPublicData,
} from "./util/method";
import {getUseData, getModelData} from "./util/getData"
import {mapInit, addPMData, baseMapInit, addMapData, addKnownMap, dataOnMap} from "./util/map";
import globalConstant from './util/global';
import message from './util/message';
import splitPane from 'vue-splitpane'

Vue.component('split-pane', splitPane);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(ElementUI);
Vue.use(Vuetify);
Vue.use(N3Components);
Vue.use(Vuelidate);
Vue.use(echarts);
Vue.use(vueGridLayout);
Vue.use(VueFullPage);


Vue.prototype.$URL = URL;
Vue.prototype.$staticData = staticData;
Vue.prototype.$globalConstant = globalConstant;
Vue.prototype.$publicModelOption = publicModelOption;
Vue.prototype.$Msg = message;
Vue.prototype.$Bus = Bus;
Vue.prototype.$axios = {
    get,
    post,
    post1,
    postAdvanced,
    patch,
    put,
    remove,
    post2
};//定义axios标签
Vue.prototype.$cookie = {
    getCookie,
    setCookie,
    delCookie
};//定义cookie标签
Vue.prototype.$date = {
    formatTime,
    dateFormat
};
Vue.prototype.$changeChart = {
    toLinechartData: toLineChartData,
    toPiechartData: toPieChartData,
    toBarchartData: toBarChartData,
    toBarbarChartData: toBarbarChartData,
    toGuageChartData: toGuageChartData

};
Vue.prototype.$method = {
    getOptionData: getOptionData,
    getField: getField,
    // getModelData: getModelData,
    getDataOfOption: getDataOfOption,
    xmlToJson: xmlToJson,
    stringToXml: stringToXml,
    getMyDataField: getMyDataField,
    formatFileSize: formatFileSize,
    // getTotalPublicData: getTotalPublicData
};
Vue.prototype.$getData = {
    getModelData: getModelData,
    getUseData: getUseData
};
Vue.prototype.$map = {
    mapInit: mapInit,
    addPMData: addPMData,
    baseMapInit: baseMapInit,
    addMapData: addMapData,
    addKnownMap: addKnownMap,
    // dataOnMap:dataOnMap
};
//定义服务器url
Vue.prototype.$serverUrl = "http://dn01:13000/dldsj/";
Vue.config.productionTip = false;

function guardRoute(to, from, next) {
    if (sessionStorage.getItem("userid") != null) {
        next();
        return true;
    } else {
        Bus.$emit("alertModalParams", {
            alertType: "error",
            alertDescription: "该页面需要先登录~"
        });
        next({
            path: from.path
        });
        return false;
    }
}

// 路由配置
const RouterConfig = {
    mode: 'hash',
    routes: routers
};
const router = new VueRouter(RouterConfig)

router.beforeEach((to, from, next) => {
    let currentPageTitle = to.meta.title;
    if (to.matched.some(m => m.meta.requireAuth)) {
        if (!guardRoute(to, from, next)) {
            currentPageTitle = from.meta.title;
        }
    } else {
        next();
    }
    util.title(currentPageTitle)//设置网页标题
});

router.afterEach((to, from, next) => {
    window.scrollTo(0, 0)
});
debugger;
/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    router,
    resource,
    template: '<App/>',
    components: {App}
});

export default router;
