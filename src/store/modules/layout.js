/* eslint-disable no-unused-vars */
import * as types from '../mutation-types'
import {appRouter,userRouter} from '../../router'
import router from "../../main";
// initial state
const state = {
    ready: false,
    routers: [],
    appRouter: [],
    openedMenuNameList: [],
    menuList: [],//与显示的导航栏一致
    menuList_all: [],
    tabList: [],
    pageOpenedList: [],
    activeTab: null,
    currentPath: [],  // 面包屑数组
    menuTheme: null, // 主题
    theme: null,
    keepAliveList: ''
}

// getters
const getters = {}

// actions
const actions = {
    initial({dispatch, commit, state}, data) {
        dispatch('initLayout').then(() => {
            dispatch('setMenuList').then(() => {
                dispatch('setTabList').then(() => {
                    commit(types.SET_LAYOUT_STATUS, true)
                })
            })
        })
    },
    initLayout({dispatch, commit, state}, data) {
        commit(types.INIT_LAYOUT)
    },
    setMenuList({dispatch, commit, state}, data) {
        commit(types.SET_MENU_LIST)
    },
    setTabList({dispatch, commit, state}, data) {
        commit(types.SET_TAB_LIST)
    },
    setCurrentPath({dispatch, commit, state}, currentPageName) {
        let currentPath = [];
        let openedMenuList = state.menuList.filter(menu => {
            if (!menu.children) {
                return menu.name === currentPageName
            } else {
                return menu.children.some((child) => {
                    if (child.name === currentPageName) {
                        currentPath.push(child)
                    }

                    return child.name === currentPageName
                })
            }
        });
        //面包屑显示数据资源时 只显示子页面名称
        // if (openedMenuList[0] && openedMenuList[0].name !== 'dataSource') {
        if (openedMenuList[0] && openedMenuList[0].name !== '') {
            let currentNode = {
                meta: {
                    title: openedMenuList[0].meta.title
                },
                // breadcrumb should not show hyperlink if the current node is the parent node
                path: openedMenuList[0].children ? '' : openedMenuList[0].path,
                name: openedMenuList[0].name
            };
            currentPath.push(currentNode);
        }

        commit(types.SET_CURRENT_PATH, currentPath.reverse());

        let openedMenuNameList = openedMenuList.map(item => {
            return item.name
        })

        commit(types.SET_OPENED_MENU_LIST, openedMenuNameList)
    },
    openTab({dispatch, commit, state}, menuInfos) {
        let menuName = menuInfos;
        let params = {}
        if (!menuInfos instanceof String) {
            menuName = menuInfos.name;
            params = menuInfos.params;
        }
        commit(types.OPEN_TAB, menuName);
        dispatch('setCurrentPath', menuName).then()
        //router.push({name:menuName,params:params})
    },
    removeTab({dispatch, commit, state}, name) {
        commit(types.REMOVE_TAB, name);
        dispatch('setCurrentPath', state.activeTab).then()
        router.push({name: state.activeTab})
    }
}

// mutations
const mutations = {
    [types.MODIFY_KEEP_ALIVE](state, args) {
        let type = args.split(',')[0];
        let name = args.split(',')[1];
        let arr = state.keepAliveList.split(',');
        if (type === 'add') {
            let bool = arr.some((item) => {
                return item === name;
            });
            if (!bool) {
                if (state.keepAliveList.length !== 0)
                    state.keepAliveList = state.keepAliveList + "," + name;
                else
                    state.keepAliveList = name;
            }

        } else {
            let result = arr.filter((item) => {
                return item !== name;
            });
            state.keepAliveList = result.join(',')
        }
    },
    [types.SET_MENU_LIST](state, list) {
        // clear menuList
        // TODO filter menuList according to the user role
        state.menuList = [];
        let temp = appRouter.slice();
        temp.map((item) => {
            if (item.children) {
                state.menuList_all.push(...item.children)
            } else {
                state.menuList_all.push(item)
            }
        })
        temp[0].children.splice(0, 3);
        state.menuList = temp;
    },
    [types.SET_TAB_LIST](state) {
        state.menuList_all.map((item) => {
            if (item.children) {
                state.tabList.push(...item.children)
            } else {
                state.tabList.push(item)
            }
        })
        let temp = userRouter.slice();
        temp.map((item)=>{
            state.tabList.push(item)
        })

    },
    [types.SET_CURRENT_PATH](state, currentPath) {
        state.currentPath = currentPath
    },
    [types.SET_OPENED_MENU_LIST](state, openedMenuNameList) {
        state.openedMenuNameList = openedMenuNameList
    },
    [types.OPEN_TAB](state, menuName) {
        let tabOpened = state.pageOpenedList.some(item => {
            return item.name === menuName
        })
        if (!tabOpened) {
            let tab = state.tabList.filter((item) => {
                return menuName === item.name
            })[0]
            state.pageOpenedList.push(tab)
        }
        state.activeTab = menuName;
    },
    [types.REMOVE_TAB](state, name) {
        if(state.pageOpenedList.length===1) return;
        let indexOfs = state.pageOpenedList.map((item, index) => {
            return (item.name === name ? index : -1);
        });
        let indexOf = indexOfs.filter(item => {
            return item !== -1;
        })[0];
        state.pageOpenedList = state.pageOpenedList.filter(item => {
            return item.name !== name
        });
        if (state.activeTab === name && state.pageOpenedList.length > 0) {
            if (state.pageOpenedList.length > indexOf) {
                //激活关闭页面的下一个页面
                state.activeTab = state.pageOpenedList[indexOf].name;
            } else if (indexOf - 1 >= 0 && state.pageOpenedList.length > indexOf - 1) {
                //激活关闭页面的上一个页面
                state.activeTab = state.pageOpenedList[indexOf - 1].name;
            }
        } else if (state.pageOpenedList.length === 0) {
            state.activeTab = null;
        }
    },
    [types.SET_LAYOUT_STATUS](state, status) {
        state.ready = status
    },
    //TODO 修改首次打开的活动页面
    [types.INIT_LAYOUT](state, name) {
        state.appRouter = appRouter.slice()
        //state.pageOpenedList = [appRouter[0]]
        state.currentPath = [
            {
                meta: {
                    title: '首页'
                },
                path: '',
                name: 'home_index'
            }
        ]
        state.menuTheme = localStorage.menuTheme ? localStorage.menuTheme : 'dark' // 主题
        state.theme = localStorage.theme ? localStorage.theme : 'b'
    },
    changeTheme(state, theme) {
        state.menuTheme = theme
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
