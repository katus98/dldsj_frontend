let icon1 = require ('@/assets/layout/icon1.png')
let icon1_1 = require ('@/assets/layout/icon1_1.png')
let icon1_2 = require ('@/assets/layout/icon1_2.png')
let icon1_3 = require ('@/assets/layout/icon1_3.png')
let icon1_4 = require ('@/assets/layout/icon1_4.png')
let icon2 = require ('@/assets/layout/icon2.png')
let icon2_1 = require ('@/assets/layout/icon2_1.png')
let icon2_2 = require ('@/assets/layout/icon2_2.png')
let icon3 = require ('@/assets/layout/icon3.png')
let icon4 = require ('@/assets/layout/icon4.png')
let icon4_1 = require ('@/assets/layout/icon4_1.png')
let icon4_2 = require ('@/assets/layout/icon4_2.png')
let icon4_3 = require ('@/assets/layout/icon4_3.png')
let icon5 = require ('@/assets/layout/icon5.png')
let icon5_1 = require ('@/assets/layout/icon5_1.png')
let icon5_2 = require ('@/assets/layout/icon5_2.png')
let icon5_3 = require ('@/assets/layout/icon5_3.png')
let icon6 = require ('@/assets/layout/icon6.png')

export {
    icon1, icon1_1, icon1_2, icon1_3, icon1_4, icon2, icon2_1, icon2_2, icon3, icon4, icon4_1, icon4_2, icon4_3, icon5, icon5_1, icon5_2, icon5_3, icon6
}