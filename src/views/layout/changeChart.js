let keyWords = ['sum', 'count', 'mean', 'min', 'max', 'variance', 'medium'];

function toMultipleSeries(chartData, type) {
    // console.log(chartData)
    let keys = Object.keys(chartData[0]);
    let xName = keys.filter(key => keyWords.indexOf(key) < 0)[0];
    keys = keys.filter(key => key !== xName);
    let series = [];
    let name2series = {};
    for (let i = 0; i < keys.length; i++) {
        let s = {name: keys[i], type: type, data: []};
        series.push(s);
        name2series[keys[i]] = s;
    }
    // console.log(series);
    let xAxis = [];
    for (let i = 0; i < chartData.length; i++) {
        xAxis.push(chartData[i][xName]);
        for (let k = 0; k < keys.length; k++) {
            name2series[keys[k]].data.push(chartData[i][keys[k]]);
        }
    }
    let interval = 0;
    if (xAxis.length > 15) {
        interval = parseInt(xAxis.length / 15) - 1;
    }
    return {
        title: {
            text: ''
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data: keys,
            bottom: 'bottom'
        },
        toolbox: {
            //保存为图
            // feature: {
            //     saveAsImage: {}
            // }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '15%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: true,
                data: xAxis,
                axisLabel: {
                    interval: interval,
                    rotate: 40
                }
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: series
    };
}


export function toLineChartData(chartData) {
    return toMultipleSeries(chartData, 'line');
    // let keys = Object.keys(chartData[0]);
    // let xName = keys.filter(key => keyWords.indexOf(key) < 0)[0];
    // keys = keys.filter(key => key !== xName);
    // let series = [];
    // let name2series = {};
    // for (let i = 0; i < keys.length; i++) {
    //   let s = {name: keys[i], type: "line", data: []};
    //   series.push(s);
    //   name2series[keys[i]] = s;
    // }
    // let xAxis = [];
    // for (let i = 0; i < chartData.length; i++) {
    //   xAxis.push(chartData[i][xName]);
    //   for (let k = 0; k < keys.length; k++) {
    //     name2series[keys[k]].data.push(chartData[i][keys[k]]);
    //   }
    // }
    // return {
    //   title: {
    //     text: ''
    //   },
    //   tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //       type: 'cross',
    //       label: {
    //         backgroundColor: '#6a7985'
    //       }
    //     }
    //   },
    //   legend: {
    //     data: keys,
    //     bottom: 'bottom'
    //   },
    //   toolbox: {
    //     feature: {
    //       saveAsImage: {}
    //     }
    //   },
    //   grid: {
    //     left: '3%',
    //     right: '4%',
    //     bottom: '15%',
    //     containLabel: true
    //   },
    //   xAxis: [
    //     {
    //       type: 'category',
    //       boundaryGap: false,
    //       data: xAxis,
    //     }
    //   ],
    //   yAxis: [
    //     {
    //       type: 'value'
    //     }
    //   ],
    //   series: series
    //   // series: [
    //   //   {
    //   //     name: yData[0],
    //   //     type: 'line',
    //   //     areaStyle: {normal: {}},
    //   //     data: chartData[2]
    //   //   },
    //   //   {
    //   //     name: yData[1],
    //   //     type: 'line',
    //   //     areaStyle: {normal: {}},
    //   //     data: chartData[3]
    //   //   },
    //   //   {
    //   //     name: yData[2],
    //   //     type: 'line',
    //   //     areaStyle: {normal: {}},
    //   //     data: chartData[4]
    //   //   },
    //   //   {
    //   //     name: yData[3],
    //   //     type: 'line',
    //   //     areaStyle: {normal: {}},
    //   //     data: chartData[5]
    //   //   }
    //   // ]
    // };
}

export function toPieChartData(chartData, seriesName) {
    seriesName = seriesName || 'mean';
    let keys = Object.keys(chartData[0]);
    let xName = keys.filter(key => keyWords.indexOf(key) < 0)[0];
    let data = [];
    let series = [{
        name: '',
        type: 'pie',
        radius: '55%',
        center: ['50%', '50%'],
        data: data,
        itemStyle: {
            emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    }];
    let xAxis = [];
    for (let i = 0; i < chartData.length; i++) {
        xAxis.push(chartData[i][xName]);
        data.push({value: chartData[i][seriesName], name: chartData[i][xName]});
    }

    return {
        title: {
            // text: '某站点用户访问来源',
            // subtext: '纯属虚构',
            x: 'center',
            right: 50
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            right: 'right',
            bottom: 10,
            data: xAxis
        },
        series: series
    };
}

export function toBarChartData(chartData) {
    return toMultipleSeries(chartData, 'bar');
}

export function toBarbarChartData(chartData) {
    let barsData = toMultipleSeries(chartData, 'bar');
    let tramsData = toMultipleSeries(chartData, 'bar');
    barsData.xAxis = tramsData.yAxis;
    barsData.yAxis = tramsData.xAxis;
    return barsData;
}

export function toGuageChartData(chartData) {
    let guaData = {};
    guaData.tooltip = {formatter: "{a} <br/>{b} : {c}" + chartData[0].unit};
    guaData.toolbox = {feature: {restore: {}, saveAsImage: {}}};
    guaData.series = [{
        name: chartData[0].sName,
        type: 'gauge',
        radius: '80%',
        axisLine: {            // 坐标轴线
            lineStyle: {       // 属性lineStyle控制线条样式
                width: 5
            }
        },
        splitLine: {           // 分隔线
            length: 10,         // 属性length控制线长
            lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                color: 'auto'
            }
        },
        detail: {formatter: '{value} '+ chartData[0].unit, fontSize:10},
        data: [{value: chartData[0].value, name: chartData[0].description}]
    }];
    return guaData;
}
