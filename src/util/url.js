const URL = {
    //get
    pubicDataUrl: '/data/list',
    personalDataUrl: '/user/data',
    publicModelUrl: '/parallel/getPublic',
    personalModelUrl: '/parallel/getMine',
    // myPersonalModelUrl: '/user/getMineModel',
    publicDataFieldUrl: '/data/field/',
    publicDataDetailUrl: '/data/detail/',
    publicDataAddByIndexUrl:'/data/addByIndex',
    publicDataAddPaper:'/data/paper/add',
    publicDataDeletePaper:'/data/paper/delete',
    updatePublicDataUrl:'/data/update',
    userDataFieldUrl: '/user/data/field/',
    previewPublicDataUrl: '/data/preview/',
    previewUserDataUrl: '/user/data/preview/',
    modelMonitorUrl: '/parallel/monitor/',
    rasterToolMonitorUrl: 'raster/task/monitor/',
    modelInfoUrl: '/parallel/get/',
    previewParallelResultUrl: '/parallel/result/preview/',
    userParallelJobUrl: '/parallel/jobs/user',
    downloadParallelJobResultUrl: '/parallel/result/download/',
    getInfoResultImage:'/parallel/resultImage/getInfo/',
    uploadResultImage:'/parallel/resultImage/upload/',
    deleteResultImage:'/parallel/resultImage/delete/',
    downloadWord:'/parallel/word/get/',
    monitor: '/wf/monitor/',
    geeTableDataUrl: '/wf/getUserDags',
    previewGeeResultUrl: '/wf/result/',
    downloadGeeProjectResultUrl: '/wf/result/',
    selfDataList: '/userself/list/',
    downloadMyData: '/userself/download/',
    deleteMyData: '/userself/delete/',
    modelLogUrl: '/parallel/log/',
    modelUpdateUrl:'/parallel/update',
    vimModelUrl:'/parallel/vim/',
    downloadDataUrl: '/user/result/download/',
    previewMyDataUrl: '/userself/preview/',
    myDataFieldsUrl: 'userself/fields/',

    userSpaceAllFileUrl: '/userspace/all?path=',
    userSpaceAllFileUrl2: '/userspace/all2?path=',
    newFolderUrl: '/userspace/create?path=',
    downloadFileUrl: '/userspace/download?path=',
    moveFileAndDirUrl: '/userspace/move',
    userSpaceFieldsUrl: '/userspace/preview/fields?path=',
    userSpaceDataUrl: '/userspace/preview/?path=',
    publicDataTreeUrl: '/classification/data',
    rasterDataTreeUrl: '/classification/raster',
    modelDataTreeUrl: '/classification/model',
    personalDataTreeUrl: '/classification/personal/data',
    personalModelTreeUrl: '/classification/personal/model',
    chartProjectDataUrl: '/userspace/preview/chart?path=',
    searchPersonalDataByFilterUrl: '/classification/search/personal/data',
    searchPublicDataByFilterUrl: '/classification/search/public/data',
    searchPersonalModelByFilterUrl: '/classification/search/personal/model',
    searchPublicModelByFilterUrl: '/classification/search/public/model',
    showTileInfoUrl: 'jobs/vectorTile/metaInfo/',


    //post
    loginUrl: '/user/login',
    logoutUrl: '/user/logout',
    registerUrl: '/user/register',
    rasterDataUrl: '/raster/data/search',
    rasterDetail:'/raster/data/detail/',
    rasterSearchbyExtent:'/raster/data/searchByExtent',
    modelRegisterUrl: '/parallel/register',
    modelRegisterWithoutXML:'/parallel/registerWithoutXML',
    modelRegisterInfo:'/parallel/registerInfo',
    modelEditableInfo:'/parallel/getEditableInfo/',
    useModelUrl: '/parallel/use/',
    startStatisticsModelUrl: '/parallel/data/stat/',
    startFilterModelUrl: '/parallel/data/filter/',
    startGeeModelUrl: '/wf/post',
    //用户修改头像图片、信息、密码和模型公开设置
    userAvatarUpdateUrl: '/user/reset/pic',
    userInfoUpdateUrl: 'user/reset/info',
    userPasswordUpdateUrl: 'user/reset/pwd',
    userModelPublicUpdateUrl: 'user/reset/model',

    dataUploadUrl: '/userself/upload',
    killJobUrl: '/parallel/kill/',
    spatialGetFieldUrl: '/spatial/get/fieldsinfo',
    spatialUploadUrl: '/spatial/create/dataset',
    spatialPersonalDataUrl: '/spatial/search',
    publicDataChooseUrl: '/classification/data/class',
    rasterDataChooseUrl: '/classification/raster/class',
    modelDataChooseUrl: '/classification/model/class',
    rasterVolumeUrl: '/classification/raster/volume',
    publicDataVolumeUrl: '/classification/data/volume',

    getRootPath:'userspace/path',
    uploadFileUrl: 'userspace/upload',
    updateModelPicUrl: 'parallel/update/pic',
    updateModelExeUrl:'parallel/update/exe',
    //创建矢量切片
    createTileUrl: 'jobs/vectorTile/create/',

    //remove
    deleteParallelJobUrl: '/parallel/jobs/',
    deleteModelUrl: '/parallel/unregister/',
    deleteFileUrL: '/userspace/delete',
    deleteDataset: '/spatial/delete/',

    //DataSpatial 数据集导出
    datasetExportUrl: '/spatial/export',
    //DataSpatial 数据集导入监控
    datasetLoaderMonitor: '/spatial/load/monitor',
    datasetLoaderLog: '/spatial/load/log/',
    datasetExportMonitor: '/spatial/export/monitor',
    datasetLoaderDelete: '/spatial/load/delete/',
    datasetExportDelete: '/spatial/export/delete/',

    //工作流，模版、监控
    workFlowSubmit:'/workflow/tasks/submit',
    workFlowTaskList:'/workflow/tasks/all?pageSize=',
    workFlowTask:'/workflow/tasks/',
    workFlowTaskDelete:'/workflow/tasks/delete/',
    workFlowTaskJobs:'/workflow/jobs/',
    workFlowJobLog:'/workflow/job/log/',
    workFlowDagList:'/workflow/dags/all',
    workFlowDagInfo:'/workflow/dags/',
    workFlowDagLoad:'/workflow/dags/load/',
    workFlowDagSave:'/workflow/dags/save',
    workFlowDagDelete:'/workflow/dags/delete/',
    workFlowOutputShow:function (params1,params2) {
        return '/workflow/jobs/'+params1+'/'+params2;
    },
    //获取公共数据缩略图
    //
    publicDataPic:'http://dn01:13000/dldsj/static/img/',

    //获取公共数据与个人数据集中的文件路径
    getDataHDFSPath:'data/path/hdfs/',
    publicDatainfo:'/data/get/',
    getSpotKeywords:'/data/keywordsList/',
    paperDownload:'/data/download/paper/',
    publicDatapreview:function (dataId) {
          return '/data/preview/'+dataId+'/geojson'
    },
    getTaskResultUrl:'/parallel/result/url/'
};
export default URL;

