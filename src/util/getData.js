import axios from 'axios';


//获取数据
//增加根据关键词获取数据的内容,keys可缺省，缺省显示全部
export function getUseData(type, pageProps, selectArr, callback,keys) {
    let result = {};
    if (type === "publicData") {
        getPublicData(pageProps, selectArr, callback,keys);
    } else if (type === "rasterData") {
        getRasterData(pageProps, selectArr, callback,keys);
    } else if (type === "personalData") {
        getPersonalData(pageProps, selectArr, callback);
    }
    return result;
}

//获取公共数据
function getPublicData(pageProps, selectArr, callback,keys) {
    let result = {};
    if(!keys) keys='';
    selectArr['keywords']=keys;
    let postUrl = '/classification/data/class?pageNo=' + pageProps.pageNo + '&pageSize=' + pageProps.pageSize;
    axios.post(postUrl, selectArr)
        .then(function (totalPublicData) {
            result = totalPublicData.data;
            callback(result);
        }).catch(function (error) {
        console.log(error);
    })
}

//获取影像数据
function getRasterData(pageProps, selectArr, callback,keys) {
    let result = {};
    if(!keys) keys='';
    selectArr['keywords']=keys;
    let postUrl = '/classification/raster/class?pageNo=' + pageProps.pageNo + '&pageSize=' + pageProps.pageSize;
    axios.post(postUrl, selectArr)
        .then(function (totalPublicData) {
            result = totalPublicData.data;
            callback(result);
        }).catch(function (error) {
        console.log(error);
    })
}

//获取个人数据
function getPersonalData(pageProps, selectArr, callback) {
    let result = {};
    let url = '/spatial/search?pageNo=' + pageProps.pageNo + '&pageSize=' + pageProps.pageSize;
    axios.post(url,selectArr)
        .then(function (personalData) {
            result = personalData.data;
            callback(result);
        }).catch(function (error) {
        console.log(error);
    })
}


//获取模型
//增加根据关键词获取数据的内容,keys可缺省，缺省显示全部
export function getModelData(type, pageProps, selectArr, callback,keys) {
    let result = {};
    if (type === "publicModel") {
        getPublicModel(pageProps, selectArr, callback,keys);
    } else if (type === "personalModel") {
        getPersonalModel(pageProps, selectArr, callback,keys);
    }
    return result;
}

//获取公共模型
function getPublicModel(pageProps, selectArr, callback,keys) {
    let result_1 = {};
    if(!keys) keys='';
    selectArr['keywords']=keys;
    let url = '/classification/model/class?pageNo=' + pageProps.pageNo + '&pageSize=' + pageProps.pageSize;
    axios.post(url,selectArr)
        .then(function (publicModel) {
            result_1 = publicModel.data;
            callback(result_1);
        }).catch(function (error) {
        console.log(error)
    });
}

//获取个人模型
function getPersonalModel(pageProps, selectArr, callback,keys) {
    let result_1 = {};
    if(!keys) keys='';
    let url = '/parallel/getMine?pageNo=' + pageProps.pageNo + '&pageSize=' + pageProps.pageSize+
        '&keywords='+keys;
    axios.get(url)
        .then(function (personalModel) {
            result_1 = personalModel.data;
            callback(result_1);
        }).catch(function (error) {
        console.log(error)
    });
}
