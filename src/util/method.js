import axios from 'axios';

export function getOptionData(type, callback) {
    let result = [];
    if (type === "publicData") {
        getPublicData(callback);
    } else if (type === "personalData") {
        getPersonal(callback);
    } else if (type === "all") {
        getAll(callback);
    } else if (type === "addNew") {
        getOptionNew(callback);
    } else if (type === "addM") {
        getOptionM(callback);
    }
    return result;
}

export function getMyDataField(id, callback) {
    let fieldList = [];
    axios.get('userself/fields/' + id)
        .then(function (res) {
            let resultList = res.data.body;
            for (let listData of resultList) {
                if (listData !== null) {
                    let option = {};
                    option.field = Object.keys(listData)[0];
                    fieldList.push(option);
                }
            }
            callback(fieldList);
        }).catch(function (err) {
        console.log(err);
    })
}



function getPublicData(callback) {
    let result_1 = [];
    axios.get('/data/list?offset=0&size=100')
        .then(function (publicData) {
            let resultList_1 = publicData.data.body.data;
            for (let listData of resultList_1) {
                if (listData !== null) {
                    if (listData.volume !== 0) {
                        let option = {};
                        option.value = listData.data.pk_meta_id;
                        option.label = listData.data.meta_name;
                        result_1.push(option);
                    }
                }
            }
            callback(result_1);
        }).catch(function (error) {
        console.log(error)
    });
}


function getPersonal(callback) {
    let result_2 = [];
    axios.get('/user/data?offset=0&size=100')
        .then(function (personalData) {
            let resultList_1 = personalData.data.body.data;
            for (let listData of resultList_1) {
                if (listData !== null) {
                    if (listData.volume !== 0) {
                        let option = {};
                        option.value = listData.data.id;
                        option.label = listData.data.name;
                        result_2.push(option);
                    }
                }
            }
            callback(result_2);
        }).catch(function (error) {
        console.log(error)
    });
}

function getAll(callback) {
    let pub = [];
    let personal = [];

    function callback_public(res) {
        pub = res;
        callback(pub.concat(personal));
    }

    function callback_personal(res) {
        personal = res;
        getPublicData(callback_public);
    }

    getPersonal(callback_personal);
}

function getOptionNew(callback) {
    let pub = [];
    let personal = [];
    let newOption = [{value: '空气质量指数', label: '空气质量指数'}, {
        value: '平均PM2.5',
        label: '平均PM2.5'
    }, {value: '气温', label: '气温'}, {value: '天气常见参数', label: '天气常见参数'}, {
        value: '污染大气观测指数',
        label: '污染大气观测指数'
    }, {value: '空气相对湿度', label: '空气相对湿度'},];

    function callback_public(res) {
        pub = res;
        callback(pub.concat(personal, newOption));
    }

    function callback_personal(res) {
        personal = res;
        getPublicData(callback_public);
    }

    getPersonal(callback_personal);
}

function getOptionM(callback) {
    let pub = [];
    let personal = [];
    let newOption = [{"value": "100", "label": "PM2.5分布图"}, {
        "value": "101",
        "label": "交通拥堵情况图"
    }];

    function callback_public(res) {
        pub = res;
        callback(pub.concat(personal, newOption));
    }

    function callback_personal(res) {
        personal = res;
        getPublicData(callback_public);
    }

    getPersonal(callback_personal);
}


export function getDataOfOption(type, callback) {
    let result = [];
    if (type === "publicOption") {
        getPublicPreview(callback);
    } else if (type === "all") {
        getAllPreview(callback);
    } else if (type === "personalOption") {
        getPersonalPreview(callback);
    }
    return result;
}

function getAllPreview(callback) {
    let pub = [];
    let personal = [];

    function callback_public(res) {
        pub = res;
        callback(pub.concat(personal));
    }

    function callback_personal(res) {
        personal = res;
        getPublicPreview(callback_public);
    }

    getPersonalPreview(callback_personal);
}

function getPublicPreview(callback) {
    axios.get('/data/list?offset=0&size=100')
        .then(function (personalData) {
            let resultList_1 = personalData.data.body.data;
            callback(resultList_1);
        }).catch(function (error) {
        console.log(error)
    });
}

function getPersonalPreview(callback) {
    axios.get('/user/data?offset=0&size=100')
        .then(function (personalData) {
            let resultList_1 = personalData.data.body.data;
            callback(resultList_1);
        }).catch(function (error) {
        console.log(error)
    });
}


export function getField(tableId, callback) {
    let result = [];
    if (tableId === '空气质量指数') {
        result = [{field: "Time1", fieldName: "Time"}, {field: "AQI", fieldName: "AQI"}];
        return result;
    } else if (tableId === '气温') {
        result = [{field: "Time", fieldName: "Time"}, {field: "Tem", fieldName: "Tem"}];
        return result;
    } else if (tableId === '平均PM2.5') {
        result = [{field: "Time2", fieldName: "Time"}, {field: "PM", fieldName: "PM"}];
        return result;
    } else if (tableId === '污染大气观测指数') {
        result = [{field: "Kind", fieldName: "Kind"}, {field: "Value", fieldName: "Value"}];
        return result;
    } else if (tableId === '天气常见参数') {
        result = [{field: "天气", fieldName: "天气"}, {field: "风向", fieldName: "风向"}, {field: "气温", fieldName: "气温"}];
        return result;
    } else {
        getPublicDataField(tableId, callback);
    }
}

function getPublicDataField(tableId, callback) {
    let fieldList = [];
    let canSelected = false;
    let isSelected = false;
    axios.get('/data/field/' + tableId)
        .then(function (fieldData) {
            let resultList = fieldData.data.body;
            if (resultList.length > 0) {
                for (let listData of resultList) {
                    if (listData !== null) {
                        let option = {};
                        option.field = listData.description;
                        option.fieldName = listData.fieldName;
                        fieldList.push(option);
                    }
                }
            } else {
                canSelected = true;
                isSelected = true;
            }
            callback(fieldList, canSelected, isSelected);
        }).catch(function (error) {
        console.log(error)
    });
}





// function getAllModel(callback) {
//     let pub = [];
//     let personal = [];
//
//     function callback_public(res) {
//         pub = res;
//         callback(pub.concat(personal));
//     }
//
//     function callback_personal(res) {
//         personal = res;
//         getPublicModel(callback_public);
//     }
//
//     getPersonalModel(callback_personal);
// }

export function xmlToJson(xml) {
    // Create the return object
    let parser = new DOMParser();
    let xmlObject = parser.parseFromString(xml, "text/xml");
    let obj = {};
    if (xmlObject.nodeType == 1) { // element
        // do attributes
        if (xmlObject.attributes.length > 0) {
            obj["@attributes"] = {};
            for (let j = 0; j < xmlObject.attributes.length; j++) {
                let attribute = xmlObject.attributes.item(j);
                obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xmlObject.nodeType == 3) { // text
        obj = xmlObject.nodeValue;
    }
    // do children
    if (xmlObject.hasChildNodes()) {
        for (let i = 0; i < xmlObject.childNodes.length; i++) {
            let item = xmlObject.childNodes.item(i);
            let nodeName = item.nodeName;
            if (typeof(obj[nodeName]) == "undefined") {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof(obj[nodeName].length) == "undefined") {
                    let old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
}


export function stringToXml(xml) {
    let parser = new DOMParser();
    let xmlObject = parser.parseFromString(xml, "text/xml");
    let Minfo = xmlObject.children[0].attributes;
    let finalInfo = {};
    for (let M of Minfo) {
        finalInfo[M.nodeName] = M.value;
    }
    return finalInfo;
}


export function formatFileSize(cellValue) {
    let temp = "0KB";
    if (!cellValue) {
        return temp;
    } else if (cellValue < 1024) {
        return cellValue + 'B';
    } else if (cellValue < (1024 * 1024)) {
        temp = cellValue / 1024;
        temp = temp.toFixed(2);
        return temp + 'KB';
    } else if (cellValue < (1024 * 1024 * 1024)) {
        temp = cellValue / (1024 * 1024);
        temp = temp.toFixed(2);
        return temp + 'MB';
    } else if (cellValue < (1024 * 1024 * 1024 * 1024)) {
        temp = cellValue / (1024 * 1024 * 1024);
        temp = temp.toFixed(2);
        return temp + 'GB';
    } else {
        temp = cellValue / (1024 * 1024 * 1024 * 1024);
        temp = temp.toFixed(2);
        return temp + 'T';
    }
}


