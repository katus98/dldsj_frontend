const message = {
    tableEmptyMsg: '获取列表数据失败!',

    userLogoutSuccessMsg: '用户注销成功',
    userLogoutFailedMsg: '用户注销失败!',
    userLoginSuccessMsg: '登录成功',
    userLoginFailedMsg: '用户登录失败!',
    userLoginErrorMsg: '用户名或密码错误',

    CheckAutoMsg: 'You must agree to continue!',
    nameCheckMaxLenMsg: '用户名最多只能',
    nameCheckMinLenMsg: '用户名最少需要',
    nameRequireLenMsg: '必须填写用户名!',
    passwordRequireMsg: '必须填写密码!',

    modelPramasEmptyMsg: '模型参数获取失败!',
    modelSubmitSuccessMsg: '模型提交成功!请前往模型项目查看运行进度！',
    modelSubmitFailedMsg: '模型提交失败!',
    modelSubmitErrorMsg: '模型提交错误!请检查所需参数',

    geeSuccessMsg: '提交成功',
    geeFailedMsg: '运行失败，请检查',
    geePramaMsg: '请填写全部所需参数',

    mapDataEmptyMsg: '该数据为空',

    deleteFailedMsg: '删除失败!',
    deleteSuccessMsg: '删除成功!',
    deleteErrorMsg: '删除错误!请刷新列表',
    deleteCancelMsg: '已取消删除',

    workFlowEmptyMsg: '工作流模型名称不能为空',
    workFlowSaveSuccessMsg: '工作流模型保存成功',
    workFlowSaveFailedMsg: '工作流模型保存失败!',

    dashBoardEmptyMsg: '数据仪表盘名称不能为空',
    dashBoardSaveSuccessMsg: '数据仪表盘保存成功',
    dashBoardSaveFailedMsg: '数据仪表盘保存失败!',
};
export default message;