const publicModelOption = [{
    value: 'all',
    label: '全部模型',

}, {
    value: '大数据统计',
    label: '大数据统计',
    children: [{
        value: '空间数据可视化',
        label: '空间数据可视化'
    }, {
        value: '属性数据可视化',
        label: '属性数据可视化'
    }, {
        value: '时空事件可视化',
        label: '时空事件可视化'
    }]
}, {
    value: '文本数据分析',
    label: '文本数据分析',
    children: [{
        value: '文本解析',
        label: '文本解析'
    }, {
        value: '位置信息处理',
        label: '位置信息处理'
    }, {
        value: '位置语义分析',
        label: '位置语义分析'
    }]
}, {
    value: '遥感数据分析',
    label: '遥感数据分析',
    children: [{
        value: '图像分类',
        label: '图像分类'
    }, {
        value: '目标识别',
        label: '目标识别'
    }, {
        value: '图谱分析',
        label: '图谱分析'
    }]
}, {
    value: '通用基础分析',
    label: '通用基础分析',
    children: [{
        value: '空间数据操作',
        label: '空间数据操作'
    }, {
        value: '时空聚类/分类',
        label: '时空聚类/分类'
    }, {
        value: '空间回归分析',
        label: '空间回归分析'
    }, {
        value: '空间关联分析',
        label: '空间关联分析'
    }, {
        value: '时空网络分析',
        label: '时空网络分析'
    }]
}, {
    value: '轨迹数据分析',
    label: '轨迹数据分析',
    children: [{
        value: '特征分析',
        label: '特征分析'
    }, {
        value: '相似性分析',
        label: '相似性分析'
    }, {
        value: '轨迹聚类',
        label: '轨迹聚类'
    }]
}, {
    value: '时空事件分析',
    label: '时空事件分析',
    children: [{
        value: '地理事件时空格局挖掘',
        label: '地理事件时空格局挖掘'
    }, {
        value: '地理事件热点与异常挖掘',
        label: '地理事件热点与异常挖掘'
    }, {
        value: '地理事件的模拟与预警',
        label: '地理事件的模拟与预警'
    }]
}];
export default publicModelOption;
