import Main from '../views/index.vue'
import Abstract from '../views/layout/abstract.vue'
import PersonalData from '../views/home/personalData.vue'
import DatasetMonitor from '../views/home/datasetMonitor.vue'
import PublicData from '../views/home/publicData.vue'
import RasterData from '../views/home/rasterData.vue'
import Login from '../views/loginNew.vue'
import publicDataInfo from "../views/publicDataInfo.vue";
import ModelInfo from "../views/ModelInfo";
import userCenter from '../views/project/userCenter.vue'
import PublicModel from '../views/project/publicModel.vue'
import PersonalModel from '../views/project/personalModel'
import fillModelInfo from "../views/fillModelInfo";
import ModelProject from '../views/project/modelproject.vue'
import RasterProject from '../views/project/rasterProject.vue'
import DataVisible from '../views/project/dataVisible.vue'
import ToGEE from '../views/project/toGEE.vue'
import workFlow from '../views/project/workFlow.vue'
import mapProject from '../views/project/MapProject/mapProject.vue'
import personalDataSpace from '../views/project/personalDataSpace/personalDataSpace'
import RasterManage from "../views/project/RasterManage";
import toRasterManage from "../views/project/toRasterManage";
import iframeShowLayer from "../views/toAnother/iframeShowLayer";
import iframeShowMap from "../views/toAnother/iframeShowMap";
import iframeShowStore from "../views/toAnother/iframeShowStore";
import helpDocument from "../views/helpDocument";
import {icon1, icon1_1, icon1_2, icon1_3, icon1_4, icon2, icon2_1, icon2_2, icon3, icon4, icon4_1, icon4_2, icon4_3, icon5, icon5_1, icon5_2, icon5_3, icon6} from '@/assets/layout/icon.js'
export const loginRouter = {
    path: '/login',
    name: 'login',
    meta: {
        title: '登录',
    },
    icon: 'fa fa-user-circle-o fa-lg',
    component: Login,
};
export const loginRouter2 = {
    path: '*',
    redirect: '/login',
};

export const appRouter = [
    {
        meta: {
            title: '数据资源',
        },
        path: '/dataSource',
        name: 'dataSource',
        icon: icon1,
        component: Abstract,
        children: [
            {
                path: 'dataInfo',
                name: 'dataInfo',
                meta: {
                    title: '公共数据信息',
                    requireAuth: false
                },
                component: publicDataInfo,
            },
            {
                path: 'modelInfo',
                name: 'modelInfo',
                meta: {
                    title: '模型信息',
                    requireAuth: false
                },
                component: ModelInfo,
            },
            {
                path: 'fillModelInfo',
                name: 'fillModelInfo',
                meta: {
                    title: '完善模型信息',
                    requireAuth: false
                },
                component: fillModelInfo
            },
            {
                meta: {
                    title: '公共数据',
                    requireAuth: false
                },
                name: 'pubData',
                path: 'pubData',
                icon: icon1_1,
                component: PublicData
            },

            //
            //     meta: {
            //         title: '个人空间',
            //         requireAuth: false
            //     },
            //     path: '/personalDataSpace',
            //     name: 'dataSpace',
            //     icon: ' fa fa-folder-open fa-lg',
            //     component: personalDataSpace
            // },
            {
                meta: {
                    title: '影像产品',
                    requireAuth: false
                },
                name: 'rasterData',
                path: 'rasterData',
                icon: icon1_2,
                component: RasterData
            }, {
                meta: {
                    title: '个人空间'
                },
                path: 'personalDataSpace',
                name: 'personalDataSpace',
                icon: icon1_3,
                component: personalDataSpace
            },
            // {
            //     meta: {
            //         title: '个人数据集',
            //         requireAuth: false
            //     },
            //     name: 'perData',
            //     path: 'perData',
            //     icon: icon1_4,
            //     component: PersonalData
            // }
        ]
    },
    {
        meta: {
            title: '计算资源'
        },
        name: 'appManage',
        path: '/appManagement',
        icon: icon2,
        component: Abstract,
        children: [
            // {
            //   meta: {
            //     title: '数据上图',
            //     requireAuth: true
            //   },
            //   name: 'dataViewM',
            //   path: 'dataViewM',
            //   icon: 'fa fa-map fa-lg',
            //   component: DataDisplay
            // },
            {
                meta: {
                    title: '共享模型',
                    requireAuth: false
                },
                name: 'PublicModel',
                path: 'PublicModel',
                icon: icon2_1,
                component: PublicModel
            }, {
                meta: {
                    title: '个人模型',
                    requireAuth: false
                },
                name: 'PersonalModel',
                path: 'PersonalModel',
                icon: icon2_2,
                component: PersonalModel
            }
        ]
    },
    // {
    //     meta: {
    //         title: '大数据地图可视化',
    //     },
    //     requireAuth: true,
    //     name: '/dataViewM',
    //     path: 'dataViewM',
    //     icon: 'fa fa-globe fa-lg',
    //     component: DataDisplay
    // },
    {
        meta: {
            title: '交互式计算',
        },
        path: '/toGEE',
        name: 'toGEE',
        icon: icon3,
        component: ToGEE,
    },
    {
        meta: {
            title: '地图工程',
        },
        path: '/toMap',
        name: 'toMap',
        icon: icon4,
        component: Abstract,
        children: [
            {
                meta: {
                    title: '数据存储',
                    requireAuth: false
                },
                name: 'dataStorage',
                path: 'dataStorage',
                icon: icon4_1,
                component: iframeShowStore,
            },
            {
                meta: {
                    title: '图层管理',
                    requireAuth: false
                },
                name: 'layerManage',
                path: 'layerManage',
                icon: icon4_2,
                component: iframeShowLayer,
            },
            {
                meta: {
                    title: '地图管理',
                    requireAuth: false
                },
                name: 'mapManage',
                path: 'mapManage',
                icon: icon4_3,
                component: iframeShowMap,
            }
        ]
    },
    {
        meta: {
            title: '任务管理'
        },
        path: '/project',
        name: 'project',
        icon: icon5,
        component: Abstract,
        children: [
            // {
            //   meta: {
            //     title: '所有项目',
            //     requireAuth: false
            //   },
            //   name: 'allProject',
            //   path: 'allProject',
            //   icon: 'fa fa-folder-open-o fa-lg',
            //   component: AllProject,
            // },
            {
                meta: {
                    title: '模型任务',
                    requireAuth: false
                },
                name: 'modelProject',
                path: 'modelProject',
                icon: icon5_1,
                component: ModelProject,
            },
            // {
            //     //ToDo 交互项目需要修改
            //     meta: {
            //         title: '交互项目',
            //         requireAuth: false
            //     },
            //     name: 'dataDisplay',
            //     path: 'dataDisplay',
            //     icon: 'fa fa-eercast fa-lg',
            //     component: DisplayProject
            // },
            {
                //ToDo 交互项目需要修改
                meta: {
                    title: '影像任务',
                    requireAuth: false
                },
                name: 'rasterProject',
                path: 'rasterProject',
                icon: icon5_2,
                component: RasterProject
            },
            // {
            //     meta: {
            //         title: '空间化任务',
            //         requireAuth: false
            //     },
            //     name: 'datasetMonitor',
            //     path: 'datasetMonitor',
            //     icon: icon5_3,
            //     component: DatasetMonitor
            // },
            //
            // {
            //   meta: {
            //     title: '统计项目',
            //     requireAuth: false
            //   },
            //   name: 'dataExplorer',
            //   path: 'dataExplorer',
            //   icon: 'fa fa-eercast fa-lg',
            //   component: ExploreProject
            // },
        ]
    },
    {
        meta: {
            title: 'PB级影像数据管理',
        },
        path: '/toRaster',
        name: 'toRaster',
        icon: icon6,
        component: toRasterManage,
    },
    // {
    //     meta: {
    //         title: '个人空间数据管理'
    //     },
    //     path: '/personalDataSpace',
    //     name: 'dataSpace',
    //     icon: ' fa fa-folder-open fa-lg',
    //     component: personalDataSpace
    // }
];
export const userRouter = [{
    meta: {
        title: '个人中心'
    },
    path: '/userCenter',
    name: 'userCenter',
    component: userCenter,
}];
export const userCenterRouter = {
    path: '/',
    name: 'personalCenter',
    meta: {
        title: '个人中心'
    },
    component: Main,
    children: userRouter,
};
export const mainRouter = {
    path: '/',
    // redirect: '/dataSource/pubData',
    redirect: '/login',
    name: 'index',
    meta: {
        title: '首页'
    },
    component: Main,
    children: appRouter
};

export const viewRouter = {
    path: '/dataVisible',
    name: 'dataVisible',
    meta: {
        title: '城市应用服务'
    },
    component: DataVisible,
    children: []
};

export const geeRouter = {
    path: '/GEE',
    name: 'gee',
    meta: {
        title: '交互式科学计算'
    },
    component: workFlow,
    children: []
};
export const rasterRouter = {
    path: '/RasterManage',
    name: 'rasterManage',
    meta: {
        title: 'PB级影像数据管理'
    },
    component: RasterManage,
    children: []
};
export const mapRouter = {
    path: '/map',
    name: 'Mapproject',
    meta: {
        title: '地图工程'
    },
    component: mapProject,
    children: []
};


// export const userRouter=[
//     {
//         meta:{
//             title:'个人中心'
//         },
//         path:'/userCenter',
//         name: 'userCenter',
//         component:userCenter
//     }
// ];
// export const userCenterRouter = {
//     path: '/',
//     name: 'personalCenter',
//     meta: {
//         title: '个人中心'
//     },
//     component: Main,
//     children: userRouter,
// };
export const helpRouter = {
    path: '/HELP',
    name: 'help',
    meta: {
        title: '帮助文档'
    },
    component: helpDocument,
    children: []
};
export const routers = [
    loginRouter,
    mainRouter,
    viewRouter,
    geeRouter,
    rasterRouter,
    userCenterRouter,
    loginRouter2,
    mapRouter,
    helpRouter
];
